from django.db import models

class Person(models.Model):
	name = models.CharField(max_length= 150)
	email = models.CharField(max_length= 50)
	phone = models. IntegerField(max_length= 12)
	city = models.CharField(max_length= 150)
# Create your models here.
