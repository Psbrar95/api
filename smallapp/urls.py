from django.contrib import admin
from django.urls import path, include
from smallapp import views
from django.conf.urls import url


urlpatterns = [
	path('', views.Personlist, name='personlist'),
	path('addperson/',views.addperson, name= 'addperson'),
	path('api/', views.personjson),
	path('api/<int:pk>/', views.persondetail),


]
